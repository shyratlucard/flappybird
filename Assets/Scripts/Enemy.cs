﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float speed;
	
	void Update ()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Boundary")
        {
            Dead();
        }
    }

    void Dead()
    {
        Destroy(this.gameObject);
    }
}
